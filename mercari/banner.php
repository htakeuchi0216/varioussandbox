<?php
/*
$dt = '2012-07-05T22:09:28+09:00';
$ts = DateTime::createFromFormat(DateTime::ISO8601, $dt)->getTimestamp();
echo $ts; // 1341493768 と表示

$begin_str = "2014-08-13T15:00:00+0900";
$begin = DateTime::createFromFormat(DateTime::ISO8601, $begin_str); 
if (!$begin ){
    print "ERROR";
}else{
    print "OK";
}
*/
#echo gettype($begin);
#$begin->
#exit();
class Banner {
    private $begin = '';
    private $end   = '';
    private $ALLOW_IPS = ['10.0.0.1','10.0.0.2'];

    public function __construct($begin_str,$end_str) {
        $begin = DateTime::createFromFormat(DateTime::ISO8601, $begin_str);
        $end   = DateTime::createFromFormat(DateTime::ISO8601, $end_str);
        // エラーチェック
        if(!$begin || !$end ){
            throw new Exception('invalid date string');
        }
        $this->begin = $begin;
        $this->end   = $end;
    }

    public function isDisplayable($ip,$now_str){
        // now型判定
        $now = DateTime::createFromFormat(DateTime::ISO8601, $now_str);
        if(!$now ){
            throw new Exception('invalid date string');
        }
        // IP 判定
        if (in_array($ip, $this->ALLOW_IPS,true)) {
            return true;
        }
        // 日付判定
        if ($this->begin <= $now && $now <= $this->end){
            return true;
        }
        return false;
    }
}
/*
class Robot {
    private $name = '';
    public function setName($name) {
        $this->name = (string)filter_var($name);
    }
    public function getName() {
        return $this->name;
    }
}
*/

/*
$a = new Robot;
$a->setName('ロボ太郎');
$b = new Robot;
$b->setName('ロボ次郎');

echo $a->getName(); // ロボ太郎
echo $b->getName(); // ロボ次郎
echo "========\n";
*/



# echo "@@@@@@@@@";
# echo gettype($begin_str);
# echo gettype($begin);
# echo "\n";
# echo $begin->year; // 1341493768 と表示
# echo $begin->format('Y-m-d');

//DateTime::createFromFormat('Y-m-d\TH:i:sP', $date)

/*
$now_str = date(DateTime::ISO8601);
$begin_str = "2014-08-10T12:00:00+0900";
$end_str = "2014-08-13T15:00:00+0900";

$banner = new Banner($begin_str, $end_str);
#    private $ALLOW_IPS = ['10.0.0.1','10.0.0.2'];
$banner->isDisplayable("10.0.0.1",$now_str);
*/