<?php
require 'q2_banner.php';
class BannerTest extends PHPUnit_Framework_TestCase
{
    function testConstructor()
    {
        // 正常系(例外が発生しない)
        try{
            $banner = new Banner("2016-07-22T12:00:00+0900","2016-07-22T15:00:00+0900");
        }catch(Exception $e){
            $this->fail('PHPUnit fail');
        }

        // 異常系
        // 日付情報誤り
        try{
            $banner = new Banner("2016-07-22T12:00:00+0900","");
            $this->fail('Exception did not occur');
        }catch(Exception $e){
           $this->assertSame('invalid datetime string', $e->getMessage());
        }

        try{
            $banner = new Banner("","2016-07-22T12:00:00+0900");
            $this->fail('Exception did not occur');
        }catch(Exception $e){
           $this->assertSame('invalid datetime string', $e->getMessage());
        }

        try{
            $banner = new Banner("","");
            $this->fail('Exception did not occur');
        }catch(Exception $e){
           $this->assertSame('invalid datetime string', $e->getMessage());
        }

        try{
            $banner = new Banner(null,null);
            $this->fail('Exception did not occur');
        }catch(Exception $e){
           $this->assertSame('invalid datetime string', $e->getMessage());
        }

        // 開始時刻と終了時刻の関係が逆転している
        try{
            $banner = new Banner("2016-07-22T15:00:00+0900","2016-07-22T12:00:00+0900");
            $this->fail('Exception did not occur');
        }catch(Exception $e){
           $this->assertSame('invalid period', $e->getMessage());
        }

    }

    function testIsDisplayable()
    {
        $banner = new Banner("2016-07-22T12:00:00+0900", "2016-07-22T15:00:00+0900");
        // 異常系
        // 日付指定誤り
        try{
            $banner->isDisplayable("1.1.1.1","");
            $this->fail('Exception did not occur');
        }catch(Exception $e){
           $this->assertSame('invalid datetime string', $e->getMessage());
        }

        try{
            $banner->isDisplayable("1.1.1.1",null);
            $this->fail('Exception did not occur');
        }catch(Exception $e){
           $this->assertSame('invalid datetime string', $e->getMessage());
        }

        // 正常系
        $now_str = date(DateTime::ISO8601);
        $begin_str  = "2016-07-22T12:00:00+0900";
        $end_str    = "2016-07-22T15:00:00+0900";

        $banner = new Banner($begin_str, $end_str);
        $this->assertEquals( $banner->isDisplayable("10.0.0.1",$now_str) , true);
        $this->assertEquals( $banner->isDisplayable("10.0.0.2",$now_str) , true);
        $this->assertEquals( $banner->isDisplayable("10.0.0.3",$now_str) , false);
        $this->assertEquals( $banner->isDisplayable("10.10.10.10",$now_str) , false);


        $this->assertEquals( $banner->isDisplayable("10.10.10.10","2016-07-01T00:00:00+0900") , false);
        $this->assertEquals( $banner->isDisplayable("10.10.10.10","2016-07-22T11:59:00+0900") , false);
        $this->assertEquals( $banner->isDisplayable("10.10.10.10","2016-07-22T12:00:00+0900") , true);
        $this->assertEquals( $banner->isDisplayable("10.10.10.10","2016-07-22T12:00:01+0900") , true);

        $this->assertEquals( $banner->isDisplayable("10.10.10.10","2016-07-22T14:59:59+0900") , true);
        $this->assertEquals( $banner->isDisplayable("10.10.10.10","2016-07-22T15:00:00+0900") , true);
        $this->assertEquals( $banner->isDisplayable("10.10.10.10","2016-07-22T15:00:01+0900") , false);
        $this->assertEquals( $banner->isDisplayable("10.10.10.10","2016-07-22T15:01:00+0900") , false);
    }

}