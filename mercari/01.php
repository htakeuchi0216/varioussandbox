<?php

function get_random_by_weight($target,$weights){
    $total = array_sum($weights);
    $rand  = rand(1,$total);
    $index = null;
    for ($i = 0; $i < count($weights) ; $i++) {
        $rand = $rand - $weights[$i];
        if ( $rand <=0 ) {
            $index = $i;
            break;
        }
    }
    return $target[$index];    
}

function test($dbs, $weights){
    $array = [];
    for ($i = 0; $i < count($dbs) ; $i++) {
      for ($j = 0; $j < $weights[$i] ; $j++) {
          $array[] = $dbs[$i];
      }
    }
    $index = array_rand($array, 1);
    return $array[$index];
}


$databases = [ 'db1','db2','db3'];
$weights  =   [ 50,25,25];

$start = microtime(true);
for ($count = 1; $count <= 10000; $count++){
   get_random_by_weight($databases,$weights);
}
$end = microtime(true);
echo "処理時間：" . ($end - $start) . "秒";

echo "\n\n";
$start = microtime(true);
for ($count = 1; $count <= 10000; $count++){
   test($databases,$weights);
}
$end = microtime(true);
echo "処理時間：" . ($end - $start) . "秒";

exit();

$hash = [];
$hash['db1'] = 0;
$hash['db2'] = 0;
$hash['db3'] = 0;

for ($i = 0; $i < 100 ; $i++) {
   $result = get_random_by_weight($databases,$weights);
    $hash[$result] += 1;
}

print_r($hash);