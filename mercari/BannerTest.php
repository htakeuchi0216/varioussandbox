<?php
require 'banner.php';
class BannerTest extends PHPUnit_Framework_TestCase
{
    function testSample1()
    {
        $now_str = date(DateTime::ISO8601);
        $begin_str  = "2016-07-22T12:00:00+0900";
        $end_str    = "2016-07-22T15:00:00+0900";
        $target_str = "2016-08-10T12:00:00+0900";

        $ip = "10.0.0.1";
        $banner = new Banner($begin_str, $end_str);
        $this->assertEquals( $banner->isDisplayable("10.0.0.1",$now_str) , true);
        $this->assertEquals( $banner->isDisplayable("10.0.0.2",$now_str) , true);
        $this->assertEquals( $banner->isDisplayable("10.0.0.3",$now_str) , false);
        $this->assertEquals( $banner->isDisplayable("10.10.10.10",$now_str) , false);


        $this->assertEquals( $banner->isDisplayable("10.10.10.10","2016-07-01T00:00:00+0900") , false);
        $this->assertEquals( $banner->isDisplayable("10.10.10.10","2016-07-22T11:59:00+0900") , false);
        $this->assertEquals( $banner->isDisplayable("10.10.10.10","2016-07-22T12:00:01+0900") , true);

        $this->assertEquals( $banner->isDisplayable("10.10.10.10","2016-07-22T14:59:59+0900") , true);
        $this->assertEquals( $banner->isDisplayable("10.10.10.10","2016-07-22T15:00:00+0900") , true);
        $this->assertEquals( $banner->isDisplayable("10.10.10.10","2016-07-22T15:00:01+0900") , false);
        $this->assertEquals( $banner->isDisplayable("10.10.10.10","2016-07-22T15:01:00+0900") , false);
    }

    function testSample2()
    {
        try{
            $banner = new Banner("2016-07-22T12:00:00+0900","");
            $this->fail('PHPUnit fail');
        }catch(Exception $e){
           $this->assertSame('invalid date string', $e->getMessage());
        }

        try{
            $banner = new Banner("","2016-07-22T12:00:00+0900");
            $this->fail('PHPUnit fail');
        }catch(Exception $e){
           $this->assertSame('invalid date string', $e->getMessage());
        }

        try{
            $banner = new Banner("","");
            $this->fail('PHPUnit fail');
        }catch(Exception $e){
           $this->assertSame('invalid date string', $e->getMessage());
        }

        try{
            $banner = new Banner(null,null);
            $this->fail('PHPUnit fail');
        }catch(Exception $e){
           $this->assertSame('invalid date string', $e->getMessage());
        }
    }

    function testSample3()
    {
        $begin_str  = "2016-07-22T12:00:00+0900";
        $end_str    = "2016-07-22T15:00:00+0900";
        $banner = new Banner($begin_str, $end_str);
        $ip = "1.1.1.1";

        try{
            $banner->isDisplayable($ip,"");
            $this->fail('PHPUnit fail');
        }catch(Exception $e){
           $this->assertSame('invalid date string', $e->getMessage());
        }

        try{
            $banner->isDisplayable($ip,null);
            $this->fail('PHPUnit fail');
        }catch(Exception $e){
           $this->assertSame('invalid date string', $e->getMessage());
        }
    }

}