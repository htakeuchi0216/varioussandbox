# coding: UTF-8

a = 10
b = 1.2341234
c = "taguchi"
d = {"fkoji":100,"hogehoge":500}
print "age: %d" %a
print "age: %10d" %a
print "age: %010d" %a
print "price: %f" %b
print "name: %s" %c

# lesson 14 if
score = 10
# if 60 < score < 80:
if score > 60:
    print "over 60"
elif score > 40:
   print "soso"
else:
    print "NOT OVER 60"

# lesson 16
# loop
sales = [13,14,200,999]
sum = 0
for sale in sales:
   sum += sale
   print sale
else:
   print "sum" +str(sum)

for i in range(0,9):
   if i == 3:
     continue
   print i

# lesson 17 
# 辞書のループ
users = { "taguchi":100, "satou":300,"hogehoge":999}
for key,value in users.iteritems():
    print key
    print value

print "======"
for key in users.iterkeys():
    print key

for val in users.itervalues():
    print val

print "[lesson18]"
# lesson 18
# while loop
n = 0
while n < 10:
   print n
   n += 1
else:
   print "end"

print "[lesson19]"
def hello(name,num=1):
    print "HELLLOOOOO" * num
    print name
    return 99999
print hello(name="hogehoeg",num=9)

# lesson20 
print "[lesson20]"
def hello2():
    pass
hello2()


# lesson21
# map , lambda
def double(x):
   return x*x

print map(double,[2,5,8])

print map(lambda x:x * x,[2,5,8])

print "[lesson22]"
# lesson22
# オブジェクト
# クラス、インスタンス

class User(object):
   def __init__(self,name):
      self.name = name
   def greet(self):
       print "my name is %s" % self.name

# lesson 23
# 継承
class SuperUser(User):
    def shout(self):
        print "%s is Super!" %self.name
       

bob = User("Bob")
print bob.name
bob.greet()

tom = SuperUser("tom")
#print bob.name
tom.greet()
tom.shout()
   

# lesson 24
# モジュールを使う
import math,random
from datetime import date

print math.ceil(5.2)
for i in range(5):
   print random.random()

print date.today()
