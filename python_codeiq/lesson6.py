# coding: UTF-8

# lesson6 
s="abcdehg"
print len(s)
print s.find("c")

print s[2]
print s[2:5]
print s[-1]

# lesson7 型変換
print 5 + int("5")
print 5 + float("5")

age = 20
print "i am " + str(age) + "years old!"

# lesson8
sales = [255,100,353,400,'abc']
print sales
print len(sales)
print sales[2]
sales[2]= 999999
print sales[2]

print 100 in sales
print 111 in sales

print range(10)
print range(3,10)
print range(3,10,2)

# lesson9
sales = [50,100,80,45]
sales.sort()
sales.reverse()
print sales

d = "2013/12/15"
print d.split("/")
a = ["a","b","c"]
print "-".join(a)

# lesson10
# タプル：変更できないリスト
a = ( 2,5,8)
#a.reverse()
#a.sort()
b= list(a)
print a
print b
b[2]=999
c=tuple(b)
print c

# lesson11
# セット(集合型)：重複を許さない
a = set( [ 1,2,3,4,2,1])
b = set([3,4,5])
print a
print a - b 
print a | b
print a & b
print a ^ b

# lesson 12
# 辞書. key:value
sales = { "taguchi":200, "fkoji":300,"hogehoge":500}
print sales
print sales["taguchi"]

print "taguchi" in sales

print sales.keys()
print sales.values()
print sales.items()

